/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2_1;

/**
 *
 * @author natta
 */
public class Lab2_1 {

     public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println("Original Array: " + java.util.Arrays.toString(arr));

        int indexToDelete = 2;
        int[] newArr1 = deleteElementByIndex(arr, indexToDelete);
        System.out.println("Array after deleting element at index " + indexToDelete + ": " + java.util.Arrays.toString(newArr1));

        int valueToDelete = 4;
        int[] newArr2 = deleteElementByValue(newArr1, valueToDelete);
        System.out.println("Array after deleting element with value " + valueToDelete + ": " + java.util.Arrays.toString(newArr2));
    }

    private static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            // Handle the case where the index is out of bounds
            return arr;
        }

        int[] newArr = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[j] = arr[i];
                j++;
            }
        }
        return newArr;
    }

    private static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            // Handle the case where the value is not found in the array
            return arr;
        }

        int[] newArr = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[j] = arr[i];
                j++;
            }
        }
        return newArr;

    }
}
